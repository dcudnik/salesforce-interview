/**
 * Created by Bidzis on 12/14/2017.
 */

global class UniqueCustomIdsCountEmailProcessor implements Database.Batchable<sObject>, Schedulable, Database.Stateful{

    global Integer accountsWithUniqueCustomId = 0;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Custom_Id__c, Name, Phone FROM Account WHERE Custom_Id__c != null');
    }

    global void execute(Database.BatchableContext bc, List<Account> scope){
        Set<String> listOfUniqueCustomIds = new Set<String>();

        for (Account account : scope) {
            if(!listOfUniqueCustomIds.contains(account.Custom_Id__c)) {
                listOfUniqueCustomIds.add(account.Custom_Id__c);
            }
        }
        accountsWithUniqueCustomId = listOfUniqueCustomIds.size();
    }

    global void finish(Database.BatchableContext bc){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        Email_Recipient__c emailRecipient = Email_Recipient__c.getInstance();

        String[] toAddresses = new String[] {emailRecipient.Email__c};

        mail.setToAddresses(toAddresses);
        mail.setSubject('Count of unique Custom ID in accounts.');

        String bodyText = 'Count of unique Custom ID in accounts is ' + accountsWithUniqueCustomId;
        mail.setPlainTextBody(bodyText);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    global void execute(SchedulableContext sc) {
        UniqueCustomIdsCountEmailProcessor uniqueCustomIdsCountEmailProcessor = new UniqueCustomIdsCountEmailProcessor();

        Database.executeBatch(uniqueCustomIdsCountEmailProcessor);
    }
}