/**
 * Created by Bidzis on 12/5/2017.
 */

@IsTest
private class AccountUpsertTest {
    static testMethod void testUpsertTwoOrLessAccountsWithThisSameCustomIdWithoutThisSameCustomIdAccountsInDatabase() {
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < 2; i++) {
            accounts.add(new Account(Name = 'MyName ' + 1, Custom_Id__c = '1111111111111111'));
        }

        Test.startTest();
        List<Database.UpsertResult> upsertResults = Database.upsert(accounts);
        Test.stopTest();

        for (Database.UpsertResult upsertResult : upsertResults) {
            System.assert(upsertResult.isSuccess());
            System.assert(upsertResult.getErrors().size() == 0);
        }
    }
    static testMethod void testUpsertAccountWithTwoAccountsWithThisSameCustomIdInDatabase() {
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < 2; i++) {
            accounts.add(new Account(Name = 'MyName Database ' + 1, Custom_Id__c = '1111111111111111'));
        }

        upsert accounts;

        Account upsertAccount = new Account(Name = 'MyName Database', Custom_Id__c = '1111111111111111');

        Test.startTest();
        try {
            upsert upsertAccount;
        } catch (Exception e) {
            System.assert(e.getMessage().contains('It can not be more than TWO accounts with this same Custom Id!'));
        }
        Test.stopTest();

    }
    static testMethod void testUpsertTwoOrMoreAccountsWithThisSameCustomIdWithThisSameCustomIdAccountsInDatabase() {
        List<Account> accounts = new List<Account>();

        Account databaseAccount = new Account(Name = 'MyName Database', Custom_Id__c = '1111111111111111');

        upsert databaseAccount;

        for (Integer i = 0; i < 2; i++) {
            accounts.add(new Account(Name = 'MyName ' + i, Custom_Id__c = '1111111111111111'));
        }

        Test.startTest();
        try {
            upsert accounts;
        } catch (Exception e) {
            System.assert(e.getMessage().contains('It can not be more than TWO accounts with this same Custom Id!'));
        }
        Test.stopTest();
    }
    static testMethod void testUpsertAccountCustomIdWithNotCorrectNumberOfCharacters() {

        Account notEnoughAccount = new Account(Name = 'NotEnough ', Custom_Id__c = '11111111111');

        Account tooMuchAccount = new Account(Name = 'TooMuch ', Custom_Id__c = '222222222222222222222222');

        Test.startTest();
        try {
            upsert notEnoughAccount;
        } catch (Exception e) {
            System.assert(e.getMessage().contains('Custom Id must have 16 characters!'));
        }
        try {
            upsert tooMuchAccount;
        } catch (Exception e) {
            System.assert(e.getMessage().contains('Custom Id must have 16 characters!'));
        }
        Test.stopTest();
    }
}