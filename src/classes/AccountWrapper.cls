/**
 * Created by danie on 05.12.2017.
 */

global with sharing class AccountWrapper {

    public String name {get; set;}
    public String phone {get; set;}
    public String customId {get; set;}

    public AccountWrapper() {}

    public AccountWrapper(String name, String phone, String customId) {
        this.name = name;
        this.phone = phone;
        this.customId = customId;
    }
}