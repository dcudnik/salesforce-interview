/**
 * Created by Bidzis on 12/15/2017.
 */

@IsTest
public with sharing class UniqueCustomIdsCountEmailProcessorTest {

    @testSetup
    private static void setup() {
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < 5; i++) {
            accounts.add(new Account(Name='account ' + i, Custom_Id__c='121212121212121'+i, Phone='123456789'));
        }
        insert accounts;

        insert new Email_Recipient__c(Email__c = 'danielcudnik93@gmail.com');
    }

    @isTest
    private static void testSendCountUniqueCustomIdProcessing() {

        String sch = '0 0 20 * * ? *';
        Test.startTest();

        UniqueCustomIdsCountEmailProcessor uniqueCustomIdsCountEmailProcessor = new UniqueCustomIdsCountEmailProcessor();


        String jobID = System.schedule('Email with count of unique custom ID TEST', sch, uniqueCustomIdsCountEmailProcessor);

        System.debug(Limits.getEmailInvocations());
//        System.assertEquals(1, Limits.getEmailInvocations());

        Test.stopTest();

        System.assertEquals(5, [SELECT count() FROM Account WHERE Custom_Id__c != null]);
    }
}