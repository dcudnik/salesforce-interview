/**
 * Created by danie on 05.12.2017.
 */

@RestResource(urlMapping='/Account/*')
global with sharing class AccountWebservice {

    @HttpPut
    global static List<Id> insertAccounts(List<AccountWrapper> accountsWrapper) {
        List<Account> accounts = new List<Account>();
        for (AccountWrapper accountWrapper : accountsWrapper) {
            accounts.add(new Account(Name=accountWrapper.name, Phone=accountWrapper.phone, Custom_Id__c=accountWrapper.customId));
        }
        try {
            upsert accounts;
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
        return new List<Id>((new Map<Id,Account>(accounts)).keySet());
    }

    @HttpGet
    global static List<AccountWrapper> retrieveAccounts() {
        List<AccountWrapper> accountsWrapper = new List<AccountWrapper>();
        for (Account acc : [SELECT Name, Phone, Custom_Id__c FROM Account WHERE Custom_Id__c != null]) {
            accountsWrapper.add(new AccountWrapper(acc.Name, acc.Phone, acc.Custom_Id__c));
        }
        return accountsWrapper;
    }

}