/**
 * Created by Bidzis on 12/13/2017.
 */

public with sharing class InsertAccountsController {

    public List<AccountWrapper> accountsToInsert {get; set;}
    public List<Integer> iterator {get; set;}


    public InsertAccountsController() {
        this.accountsToInsert = new List<AccountWrapper>();
        this.iterator = new List<Integer>();

        addNewAccountToInsert();

    }

    public void addNewAccountToInsert() {
        this.accountsToInsert.add(new AccountWrapper());
        this.iterator.add(this.accountsToInsert.size() - 1);
    }

    public void deleteAccountFromAccountsToInsert() {
        Integer currentId = Integer.valueOf(System.currentPageReference().getParameters().get('currentIndex'));

        this.iterator.remove(accountsToInsert.size() - 1);
        this.accountsToInsert.remove(currentId);
    }

    public PageReference saveAccounts() {
    	for (Integer i = 0; i < this.accountsToInsert.size(); i++) {
    		if(String.isEmpty(this.accountsToInsert[i].name)) {
    			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Name field is required'));
    			return null;
    		}
    	}
        try {
            AccountWebservice.insertAccounts(this.accountsToInsert);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'You successful inserted accounts'));
        } catch (Exception e) {
			
        }
        return null;
    }

    public boolean hasMessages {
        get {
            return ApexPages.hasMessages();
        }
    }
    /**@description The custom error message */
    public String errorMessage {
        get {
            if (hasMessages) {
                return ApexPages.getMessages()[0].getDetail();
            }

            return null;
        }
    }
    public string alertType {
        get {
            if (hasMessages) {
                return ApexPages.getMessages()[0].getSeverity() == ApexPages.Severity.CONFIRM ? 'success' : 'error';
            }

            return 'error';
        }
        private set;
    }
}