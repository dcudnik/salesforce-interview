/**
 * Created by Bidzis on 12/5/2017.
 */

@IsTest
private class AccountWebserviceTest {
    static testMethod void testRetrieveAccounts() {
        List<AccountWrapper> testAccounts = createTestRecordsGet();

        RestRequest request = new RestRequest();
        request.requestUri = 'https://dcud-dev-ed.my.salesforce.com/services/apexrest/Account/';
        request.httpMethod = 'GET';
        RestContext.request = request;

        List<AccountWrapper> returnedAccounts = AccountWebservice.retrieveAccounts();

        for (Integer i = 0; i < testAccounts.size(); i++) {
            System.assertEquals(testAccounts[i].name, returnedAccounts[i].name);
            System.assertEquals(testAccounts[i].phone, returnedAccounts[i].phone);
            System.assertEquals(testAccounts[i].customId, returnedAccounts[i].customId);
        }
    }

    static testMethod void testInsertAccounts() {
        List<AccountWrapper> testAccounts = createTestRecordsPut();

        List<Id> returnedAccounts = AccountWebservice.insertAccounts(new List<AccountWrapper>{testAccounts.get(0)});

        System.assert([SELECT count() FROM Account WHERE Custom_Id__c = :testAccounts.get(0).customId] == 1);

        try {
            List<Id> failedAccounts = AccountWebservice.insertAccounts(new List<AccountWrapper>{testAccounts.get(1), testAccounts.get(2)});
        } catch (Exception e) {
            System.assert(e.getMessage().contains('It can not be more than TWO accounts with this same Custom Id!'));
        }


    }

    static List<AccountWrapper> createTestRecordsGet() {
        Account accountOne = new Account(Name = 'Account One', Phone = '777777777', Custom_Id__c = '1234567890123456');
        Account accountTwo = new Account(Name = 'Account Two', Phone = '777777777', Custom_Id__c = '0987654321098765');
        Account accountThree = new Account(Name = 'Account Three', Phone = '777777777', Custom_Id__c = '0987654321098765');

        List<Account> accounts = new List<Account>{
                accountOne, accountTwo, accountThree
        };

        upsert accounts;

        return new List<AccountWrapper>{
                new AccountWrapper(accountOne.Name, accountOne.Phone, accountOne.Custom_Id__c), new AccountWrapper(accountTwo.Name, accountTwo.Phone, accountTwo.Custom_Id__c), new AccountWrapper(accountThree.Name, accountThree.Phone, accountThree.Custom_Id__c)
        };
    }

    static List<AccountWrapper> createTestRecordsPut() {

        AccountWrapper accountOne = new AccountWrapper('Account One', '777777777', '1234567890123456');
        AccountWrapper accountTwo = new AccountWrapper('Account Two', '777777777', '1234567890123456');
        AccountWrapper accountThree = new AccountWrapper('Account Tree', '777777777', '1234567890123456');

        List<AccountWrapper> accounts = new List<AccountWrapper>{
                accountOne, accountTwo, accountThree
        };

        return accounts;
    }
}