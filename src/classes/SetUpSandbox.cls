public with sharing class SetUpSandbox {
    
    private String email;
    private String hour;
    private String minute;
    private String second;
    
    public SetUpSandbox(String email, String hour, String minute, String second) {
    	this.email = email;
    	this.hour = hour;
    	this.minute = minute;
    	this.second = second;
    	
    	setUpEmailCustomSetting();
    	
    	setUpSendingEmail();
    }
    
    private void setUpEmailCustomSetting() {
    	upsert new Email_Recipient__c(Name='develop', Email__c=this.email);
    }
    
    private void setUpSendingEmail() {
    	UniqueCustomIdsCountEmailProcessor uniqueCustomIdsCountEmailProcessor = new UniqueCustomIdsCountEmailProcessor();
        String sch = this.second +' '+ this.minute + ' ' + this.hour +' * * ? *';
        String jobID = System.schedule('Email with count of unique custom ID', sch, uniqueCustomIdsCountEmailProcessor);
    }
}