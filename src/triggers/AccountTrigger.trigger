/**
 * Created by danie on 05.12.2017.
 */

trigger AccountTrigger on Account (before insert, before update) {
    if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {

        Map<String, Integer> occurrenceOfCustomIdMap = new Map<String, Integer>();
		
        for (Account insertedAccount : Trigger.new) {
            if (insertedAccount.Custom_Id__c.length() != 16) {
                insertedAccount.addError('Custom Id must have 16 characters! ('+insertedAccount.Custom_Id__c+')');
            }
            if (!occurrenceOfCustomIdMap.containsKey(insertedAccount.Custom_Id__c)) {
                occurrenceOfCustomIdMap.put(insertedAccount.Custom_Id__c, 0);
            }
            occurrenceOfCustomIdMap.put(insertedAccount.Custom_Id__c, occurrenceOfCustomIdMap.get(insertedAccount.Custom_Id__c) + 1);
            Integer occurrenceOfCustomIdInDatabase = [SELECT count() FROM Account WHERE Custom_Id__c = :insertedAccount.Custom_Id__c];
            if (occurrenceOfCustomIdMap.get(insertedAccount.Custom_Id__c) + occurrenceOfCustomIdInDatabase > 2) {
                insertedAccount.addError('It can not be more than TWO accounts with this same Custom Id! ('+insertedAccount.Custom_Id__c+')');
            }
        }
    }
}