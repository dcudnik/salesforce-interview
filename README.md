### How do I get set up? ###
In build.properties file write your sandbox username and password with security token.

Open cmd or powershell in your project directory. In console type: ant deploy-all. In the end should 'build success' message appear.

To set up email address and time of sending email:
In your sandbox open developer console: Debug > Open Execute Anonymous Window.
Type: SetUpSandbox setup = new SetUpSandbox('email', 'hour', 'minute', 'second');
WHERE email - is email address to receive daily email, hour - is hour which the system sends email,
minute - is minute which the system sends email, second - is second which the system sends email

Custom VisualForce Page: In your sandbox click '+'. After page reload find Insert Accounts tab and click it.

### Who do I talk to? ###

Daniel Cudnik
danielcudnik93@gmail.com